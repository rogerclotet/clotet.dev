---
title: Família
slug: familia
description: A family manager to share lists, calendar, files, etc.
category: webapp
image: /projects/familia.png
link: https://familia.clotet.dev
repo: https://gitlab.com/rogerclotet/familia
date: 2024-06-23
---

A family manager I started back in 2020 and have rewritten from scratch twice already. Hopefully this is the last time, since it's at a point where we're actively using it with several groups and it's evolving based on feedback from real users. It has been received very well by the poeple I've shared it with, and I can see myself actively working on it for years to come.

The idea is to have groups of people you can share various things with. For instance, you can share a grocery list with your partner, a calendar with family members for special days and events, share tickets for a concert in the calendar event itself, or share expenses with a group of friends.

The feature list is meant to grow, but the central idea is to share all sorts of things with groups of people.

The tech used is Next.js for the frontend and backend, PostgreSQL for the database connected with Drizzle ORM, and Tailwind CSS for styling.
