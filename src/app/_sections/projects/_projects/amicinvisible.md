---
title: Amic Invisible
slug: amic-invisible
description: A webapp to manage Secret Santa groups with friends and family
category: webapp
image: /projects/amicinvisible.png
date: 2018-11-04
---

A webapp to manage [Secret Santa](https://en.m.wikipedia.org/wiki/Secret_Santa) groups with friends and family, built as
a prototype with plans to rewrite it in a better way in the future.

It allows you to create groups, invite people using a link or using the share dialog in your phone, decide randomly who
is each other's secret Santa, and share a "wish list" to help her or him get ideas for your gift.
