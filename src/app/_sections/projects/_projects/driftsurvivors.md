---
title: Drift Survivors
slug: drift-survivors
description: A mobile game inspired in Vampire Survivors
category: game
image: /projects/driftsurvivors.png
link: https://play.google.com/store/apps/details?id=dev.clotet.driftsurvivors
repo: https://gitlab.com/rogerclotet/drift-survivors
date: 2023-04-12
---

A mobile game inspired in Vampire Survivors, where you drive a car drifting around a desert, running over enemies, and shooting them with several weapons, in order to survive as long as possible while waves of stronger and stronger enemies come to you.

This game was developed using Godot and using custom pixel art. The architecture of the project is built with expandability in mind, and it's easy to add new weapons and upgrades to the game.

You can download and play the game on Android from the [Play Store](https://play.google.com/store/apps/details?id=dev.clotet.driftsurvivors).
