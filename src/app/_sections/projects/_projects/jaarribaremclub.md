---
title: Ja Arribarem Club
slug: jaarribaremclub
description: A website for a popular walk event
category: website
image: /projects/jaarribaremclub.png
repo: https://gitlab.com/rogerclotet/jaarribaremclub
date: 2006-03-01
---

A website for a popular walk event I've built, iterated upon, and maintained from 2006 to 2019.

It started as a static site to provide basic information of the current year's event, and since then I've rewritten it a
couple of times to what it is today: a Symfony application with an administration zone to edit and add new contents
every year and required minimal maintenance from my side.
