---
title: Blocs!
slug: blocs
description: A small puzzle game for mobile
category: game
image: /projects/blocs.png
link: https://play.google.com/store/apps/details?id=dev.clotet.Blocs
repo: https://gitlab.com/rogerclotet/blocs
date: 2020-04-28
---

A small puzzle game for mobile. The first game I published on the Google Play Store.

The gameplay is inspired by other puzzle games I tried before, but were full of ads and in-app purchases. I started 
learning game development some time before, took part in a game jam, and wanted to make a simple game to "test my 
skills", and this seemed like a good way of doing it.

It was made with Unity, art was created with Aseprite, and has local saves for high scores, last games and current 
game state, localization in Catalan, English, and Spanish, and different color schemes to choose from.

You can install and 
[play the game for free on Android](https://play.google.com/store/apps/details?id=dev.clotet.Blocs&utm_source=clotet.dev&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1), 
or play now from mobile or desktop on https://blocs.clotet.dev.
